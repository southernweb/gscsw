<!DOCTYPE html>
<html lang="en">
<head>

	<meta charset="UTF-8" />

	<?php if (is_search()) { ?>
	   <meta name="robots" content="noindex, nofollow" />
	<?php } ?>



	<title><?php wp_title('');?></title>
	<?php wp_head(); ?>

	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />



	<?php if (is_author()) { ?>
	   <meta property="og:title" content="Authors!!!" />
	<?php } ?>

	<script type="text/javascript" src="//use.typekit.com/vbh3uhi.js"></script>
	<script type="text/javascript">try{Typekit.load();}catch(e){}</script>

</head>

<body <?php body_class(); ?>>
<?php if ( function_exists( 'gtm4wp_the_gtm_tag' ) ) { gtm4wp_the_gtm_tag(); } ?>
<div id="wrapper">
		<div id="header">
			<div class="container clearfix">
				<div id="tools">
					<ul id="social_btns">
						<li class="facebook"><a href="https://www.facebook.com/groups/144040415624274/" target="_blank"><i class="fa fa-facebook-official" aria-hidden="true"></i><span>GSCSW On Facebook</span></a></li>
					</ul>
					<div class="search_form">
						<?php get_search_form( $echo ); ?>
					</div>
					<?php if ( is_user_logged_in() ) { ?>
						<a href="<?php echo wp_logout_url('/login/');  ?>" class="login_btn">Logout &raquo;</a>
					<?php } else { ?>
						<a href="/login/" class="login_btn">Member Login &raquo;</a>
					<?php } ?>
				</div>
				<div id="logo">
					<a href="<?php echo get_option('home'); ?>/"><?php bloginfo('name'); ?></a>
				</div>

				<?php wp_nav_menu( array('menu' => 'Header', 'link_before' => '<span>','link_after' => '</span>')); ?>
			</div>
		</div>

		<div class="container">
			<div id="main" class="clearfix">
