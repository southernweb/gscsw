<?php get_header(); ?>

<div id="content" class="clearfix">

	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

		<div <?php post_class() ?>>
			
			<h1 class="entry-title"><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h1>
				
			<?php the_content(); ?>
			
		</div>

	<?php endwhile; endif; ?>

</div><!-- /#content -->
	
<?php get_sidebar(); ?>
<?php get_footer(); ?>