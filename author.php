<?php 


get_header(); ?>

<div id="content" class="clearfix member-profile">

	<!-- This sets the $curauth variable -->
    <?php $curauth = (isset($_GET['author_name'])) ? get_user_by('slug', $author_name) : get_userdata(intval($author)); ?>
   	<?php 

   		$office_name = get_the_author_meta('office_name', $curauth->ID);
		$office_street = get_the_author_meta('office_address', $curauth->ID);
		$office_street_two = get_the_author_meta('office_address_two', $curauth->ID);
		$office_city = get_the_author_meta('office_city', $curauth->ID);
		$office_state = get_the_author_meta('office_state', $curauth->ID);
		$office_zip = get_the_author_meta('office_zip', $curauth->ID);
		$office_phone = get_the_author_meta('office_phone', $curauth->ID);

		$office_two_name = get_the_author_meta('office_two_name', $curauth->ID);
		$office_two_street = get_the_author_meta('office_two_address', $curauth->ID);
		$office_two_street_two = get_the_author_meta('office_two_address_two', $curauth->ID);
		$office_two_city = get_the_author_meta('office_two_city', $curauth->ID);
		$office_two_state = get_the_author_meta('office_two_state', $curauth->ID);
		$office_two_zip = get_the_author_meta('office_two_zip', $curauth->ID);
		$office_two_phone = get_the_author_meta('office_two_phone', $curauth->ID);

		$home_phone = get_the_author_meta('home_phone', $curauth->ID);
		$member_email = get_the_author_meta('email', $curauth->ID);
		$member_website = get_the_author_meta('office_website_url', $curauth->ID);
		$member_hide_email = strtolower (get_the_author_meta('hide_email', $curauth->ID));

		$practice_short = get_the_author_meta('practice_short', $curauth->ID);
		$practice_long = get_the_author_meta('practice_long', $curauth->ID); 	

		$profile_image = get_the_author_meta('cupp_upload_meta', $curauth->ID);

		
	?>
	<h1>Find A Therapist</h1>
	<div class="member-card-top">
	<?php if($profile_image){ ?>
		<div class="profile-left">	
			<img src="<?php echo $profile_image; ?> ">
		</div>
		<div class="profile-right">
		
		    <h2><?php echo $curauth->first_name; ?> <?php echo $curauth->last_name; ?></h2>
		    
		    <p><?php echo $curauth->user_description; ?></p>
	    </div>
    <?php } else { ?>
    	<h2><?php echo $curauth->first_name; ?> <?php echo $curauth->last_name; ?></h2>
		<p><?php echo $curauth->user_description; ?></p>
    <?php } ?>
    </div>
    
    
    <hr/>

	<table width="100%" border="0" cellspacing="0" cellpadding="4">
	<tbody><tr>
	<td width="33%"><strong>Office Location(s)</strong></td>
	<td width="34%"><strong>Contact Info</strong></td>
	<td width="33%"><strong>Practice Description</strong></td>
	</tr>
	<tr>
		<td valign="top"> 
			<?php if($office_name){ ?>
				<strong><?php echo $office_name; ?></strong><br>
			<?php } ?>
			<?php echo $office_street; ?><br>
			<?php if($office_street_two){ ?>
				<?php echo $office_street_two; ?><br>
			<?php } ?>
			<?php if($office_city){ ?>
				<?php echo $office_city; ?>,
			<?php } ?>
			<?php echo $office_state; ?> <?php echo $office_zip; ?> 

			<br><br>

			<?php if($office_two_name){ ?>
				<strong><?php echo $office_two_name; ?></strong><br>
			<?php } ?>
			<?php if($office_two_street){ ?>
				<?php echo $office_two_street; ?><br>
			<?php } ?>
			<?php if($office_two_street_two){ ?>
				<?php echo $office_two_street_two; ?><br>
			<?php } ?>
			<?php if($office_two_city){ ?>
				<?php echo $office_two_city; ?>,
			<?php } ?>
			<?php if($office_two_state){ ?>
				<?php echo $office_two_state; ?> <?php echo $office_two_zip; ?> 
			<?php } ?>
		</td>
		<td valign="top"> 
			<?php if ($office_phone){ ?>
				<strong>Phone:</strong> <?php echo $office_phone; ?> <br>
			<?php } ?>
			<?php if ($member_hide_email != "yes"){ ?>
				<strong>Email:</strong> <a href="mailto:<?php echo $member_email; ?>"><?php echo $member_email; ?></a><br>
			<?php } ?>
			<?php if ($member_website){ ?>
				<strong>Website:</strong> <a href="<?php echo $member_website; ?>" target="_blank"><?php echo $member_website; ?></a>
			<?php } ?>
			<br>
		</td>
		<td valign="top">
			<?php if ($practice_short){ ?>
				<?php echo $practice_short; ?> 
				<br><br>
			<?php } ?>
			<?php if ($practice_long){ ?>
				<?php echo $practice_long; ?> 
			<?php } ?>
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	</tbody>
	</table>


</div><!-- /#content -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>