<?php


// Add RSS links to <head> section
add_theme_support( 'automatic-feed-links' );

// Load jQuery
if ( !is_admin() ) {
   wp_enqueue_script('jquery');
}

if( function_exists('add_theme_support') ) {
	add_theme_support('post-thumbnails');
    add_theme_support( 'menus' );
}

// Clean up the <head>
function removeHeadLinks() {
	remove_action('wp_head', 'rsd_link');
	remove_action('wp_head', 'wlwmanifest_link');
}
add_action('init', 'removeHeadLinks');
remove_action('wp_head', 'wp_generator');

// give editor ability to edit Appearance > Menus
$role_object = get_role( 'editor' );
$role_object->add_cap( 'edit_theme_options' );


/**
 * Hide email from Spam Bots using a shortcode.
 *
 * @param array  $atts    Shortcode attributes. Not used.
 * @param string $content The shortcode content. Should be an email address.
 *
 * @return string The obfuscated email address.
 */
function wpcodex_hide_email_shortcode( $atts , $content = null ) {
  if ( ! is_email( $content ) ) {
    return;
  }

  return '<a href="mailto:' . antispambot( $content ) . '">' . antispambot( $content ) . '</a>';
}
add_shortcode( 'email', 'wpcodex_hide_email_shortcode' );



// Excerpts

function excerpt_read_more_link($output) {
 global $post;
 return $output . '</p><p><a href="'. get_permalink($post->ID) . '"> Read More &raquo;</a>';
}
add_filter('the_excerpt', 'excerpt_read_more_link');

function new_excerpt_more($excerpt) {
	return str_replace('[...]', '...', $excerpt);
}
add_filter('wp_trim_excerpt', 'new_excerpt_more');

function custom_excerpt_length( $length ) {
	return 25;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

// subpages shortcode
function subpage_shortcode($atts) {

    global $post;
    extract( shortcode_atts( array(
        'child_of' => $post->ID
    ), $atts ) );

    $attributes = array(
        'title_li' => '',
        'echo' => 0,
        'depth' => 1,
        'child_of' => $child_of
    );

    return '<ul class="sub-pages sub-pages-of-' . $post->ID . '">' . wp_list_pages($attributes) . '</ul>';

}
add_shortcode('subpages', 'subpage_shortcode');

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/members-fields.php';

/*
 * Replace WordPress logo on login page with Coal Trade logo.
 */
function custom_login_logo() {
    echo '<style type="text/css">
    h1 a { background-image: url('.get_bloginfo('template_directory').'/ui/gscsw-logo.png) !important; }
    .login h1 a { width: 120px; height: 120px; background-size: 120px; }
    </style>';
}
add_action('login_head', 'custom_login_logo');




/*
 * Versioning function to ensure css file has a unique filename per upload.
 */
function css_versioning() {
  wp_enqueue_style( 'gscsw-style', get_stylesheet_directory_uri() . '/style.css' , false,filemtime( get_stylesheet_directory() . '/style.css' ), 'all' );
}

/**
 * Enqueue scripts and styles.
 */
function gscsw_scripts() {

  add_action( 'wp_print_styles', 'css_versioning' );

  if ( is_singular() ) wp_enqueue_script( 'comment-reply' );
	wp_enqueue_style('font-awesome', get_template_directory_uri(). '/inc/font-awesome/css/font-awesome.min.css' );
    wp_enqueue_script('scripts', get_bloginfo('template_directory') . '/js/scripts.js', 'jQuery');
    wp_enqueue_script('superfish', get_bloginfo('template_directory') . '/js/superfish.js', 'jQuery');
    wp_enqueue_script('supersubs', get_bloginfo('template_directory') . '/js/supersubs.js', 'jQuery');
    wp_enqueue_script('therapist-search', get_bloginfo('template_directory') . '/js/therapist-search.js', 'jQuery', 2);

}
add_action( 'wp_enqueue_scripts', 'gscsw_scripts' );



/**
 * http://www.paulund.co.uk/create-your-own-wordpress-login-page
 */
add_action( 'wp_login_failed', 'pu_login_failed' ); // hook failed login

function pu_login_failed( $user ) {
    // check what page the login attempt is coming from
    $referrer = $_SERVER['HTTP_REFERER'];

    // check that were not on the default login page
    if ( !empty($referrer) && !strstr($referrer,'wp-login') && !strstr($referrer,'wp-admin') && $user!=null ) {
        // make sure we don't already have a failed login attempt
        if ( !strstr($referrer, '?login=failed' )) {
            // Redirect to the login page and append a querystring of login failed
            wp_redirect( $referrer . '?login=failed');
        } else {
            wp_redirect( $referrer );
        }

        exit;
    }
}

add_action( 'authenticate', 'pu_blank_login');

function pu_blank_login( $user ){
    // check what page the login attempt is coming from
    $referrer = $_SERVER['HTTP_REFERER'];

    $error = false;

    if($_POST['log'] == '' || $_POST['pwd'] == '')
    {
        $error = true;
    }

    // check that were not on the default login page
    if ( !empty($referrer) && !strstr($referrer,'wp-login') && !strstr($referrer,'wp-admin') && $error ) {

        // make sure we don't already have a failed login attempt
        if ( !strstr($referrer, '?login=failed') ) {
            // Redirect to the login page and append a querystring of login failed
            wp_redirect( $referrer . '?login=failed' );
        } else {
            wp_redirect( $referrer );
        }

    exit;

    }
}


/*
 * Hide WordPress admin bar from Subscribers.
 */
add_action('after_setup_theme', 'remove_admin_bar');

function remove_admin_bar() {
    if (!current_user_can('administrator') && !is_admin()) {
      show_admin_bar(false);
    }
}

/**
* Populate Date: Year from Current Date
*/
add_filter('gform_field_value_year_from_date', 'gw_year_from_date');
function gw_year_from_date() {
    return strtotime('+1 year');
}



/*
 * Gravity Forms expiration settings.
 */

add_action( 'profile_update', 'user_expiration_date_key', 10, 4 );
function user_expiration_date_key($user_id) {
    $user_information = get_userdata($user_id);

     $key = '_expire_user_date';
     $single = true;
     $epoch = get_user_meta( $user_id, $key, $single );
     $formatted_date = date('m-d-Y H:i:s', $epoch);

     $new_key = "user_expiration_date";
     $has_new_key = get_user_meta( $user_id, $new_key, $single );

     if(isset($has_new_key)){
        update_user_meta( $user_id, $new_key, $formatted_date );
     } else {
        add_user_meta( $user_id, $new_key, $formatted_date );
     }
}



add_action( 'gform_user_registered', 'create_expire_settings', 10, 4 );

function create_expire_settings( $user_id, $config, $entry, $user_pass ) {

        $myarray = array(
            'default_to_role' => 'inactive',
            'remove_expiry' => 1,
            'email' => 1,
            'email_admin' => 1,
        );

        add_user_meta($user_id, '_expire_user_settings' , $myarray);

        $key = '_expire_user_date';
        $single = true;
        $epoch = get_user_meta( $user_id, $key, $single );

        $formatted_date = date('m-d-Y H:i:s', $epoch);

        $new_key = "user_expiration_date";
        add_user_meta( $user_id, $new_key, $formatted_date );
        update_user_meta( $user_id, '_expire_user_expired', 'N' );
}


add_action( 'gform_user_updated', 'update_expire_settings', 10, 4 );

function update_expire_settings( $user_id, $config, $entry, $user_pass ) {

        $myarray = array(
            'default_to_role' => 'inactive',
            'remove_expiry' => 1,
            'email' => 1,
            'email_admin' => 1,
        );

        update_user_meta($user_id, '_expire_user_settings' , $myarray);
        update_user_meta( $user_id, '_expire_user_expired', 'N' );

         $key = '_expire_user_date';
         $single = true;
         $epoch = get_user_meta( $user_id, $key, $single );

         $formatted_date = date('m-d-Y H:i:s', $epoch);


         $new_key = "user_expiration_date";
         update_user_meta( $user_id, $new_key, $formatted_date );
    }

/*
 * Replace Author Title tag with custom tag.
 */
add_filter('wpseo_title', 'filter_product_wpseo_title');
function filter_product_wpseo_title($title) {
    if( is_author() ) {
        global $post;
        $author = get_user_by( 'slug', get_query_var( 'author_name' ) );
        $this_auth = $author->ID;
        $user_last = get_the_author_meta('last_name', $this_auth);
        $user_first = get_the_author_meta('first_name', $this_auth);
        $office_name = get_the_author_meta('office_name', $this_auth);

        if($office_name){
            $title = $user_first . ' ' . $user_last . ' | ' . $office_name;
        } else {
            $title = $user_first . ' ' . $user_last;
        }


    }
    return $title;
}


// Generate a 'pretty' version of expire time on page load.
add_action('wp', 'initial_epoch_date');

function initial_epoch_date() {

    if(is_page( 1077 )){

        $users = get_users();

        // var_dump(get_object_vars($user_fields));

        foreach ( $users as $user ) {
             $user_id = $user->ID;
             $key = '_expire_user_date';
             $single = true;
             $epoch = get_user_meta( $user_id, $key, $single );

             $formatted_date = date('m-d-Y H:i:s', $epoch);


             $new_key = "user_expiration_date";
             add_user_meta( $user_id, $new_key, $formatted_date );
             // update_user_meta( $user_id, $key, $formatted_date );

        }

    }

}



// add_action('wp', 'update_profile_image');

// function update_profile_image() {

//     if(is_page( 1097 )){

//         $users = get_users();

//         foreach ( $users as $user ) {
//              $user_id = $user->ID;

//              $img_url = 'http://www.gscsw.org/wp-content/uploads/2016/04/default-profile.png';

//              update_user_meta($user_id, 'cupp_meta' , $img_url);


//         }

//     }

// }



// add_action('wp', 'set_expired_settings');

// function set_expired_settings() {

//     if(is_page( 1079 )){

//         $users = get_users();

//         foreach ( $users as $user ) {
//              $user_id = $user->ID;

//              $myarray = array(
//                 'default_to_role' => 'inactive',
//                 'remove_expiry' => 1,
//                 'email' => 1,
//                 'email_admin' => 1,
//             );

//             update_user_meta($user_id, '_expire_user_settings' , $myarray);


//         }

//     }

// }
