<?php 

/* Template Name: Reading List */

get_header(); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		
	<div id="content" class="clearfix">
	
		<div <?php post_class(); ?>>
		
			<span class="entry-title hide"><?php the_title();?></span>
			
			<div class="entry-content">
				<?php the_content(); ?>
			</div>
			
			<?php 
				$categories = get_field('reading_list');
			?>
			
			<?php if($categories): ?>
				<div class="list_category">
					<?php foreach($categories as $category) : ?>
						<?php if($category['category']): ?>
						<h3><?php echo $category['category'];?></h3>
							<ul class="reading_list clearfix">
							<?php foreach($category['amazon_products'] as $product): ?>
									
									<?php if($product['product']): ?>
										<li><?php echo $product['product'];?></li>
									<?php endif; ?>
									
							<?php endforeach; ?>
							</ul>
							<hr/>
						<?php endif; ?>
				
					<?php endforeach; ?>
				</div>
			<?php endif; ?>
			
		</div>
		
	</div><!-- /#content -->

<?php endwhile; endif; ?>
	
<?php get_sidebar(); ?>
<?php get_footer(); ?>
