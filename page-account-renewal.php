<?php get_header(); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		
	<div id="content" class="clearfix">
	
		<div <?php post_class(); ?>>
		
			<span class="entry-title hide"><?php the_title();?></span>
			
			<div class="entry-content">
				<?php if ( is_user_logged_in() ) {

						get_currentuserinfo();
					    $user_info = get_userdata($current_user->ID);

						if( in_array('inactive', $user_info->roles) ) { ?>
						<h2>Your account is inactive. Use the form below to reactivate your account.</h2>
					<?php }
					} ?>

				<?php if ( !is_user_logged_in() ) { ?>	
					<?php get_template_part( 'template-parts/content', 'login' ); ?>
				<?php } else { ?>
					<?php the_content(); ?>
				<?php } ?>
			</div>
			
		</div>
		
	</div><!-- /#content -->

<?php endwhile; endif; ?>


	
<?php get_sidebar(); ?>
<?php get_footer(); ?>