<?php 

/* Template Name: Find a Therapist */

get_header(); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		
	<div id="content" class="clearfix">
	
		<div <?php post_class(); ?>>
		
			<span class="entry-title hide"><?php the_title();?></span>
			
			<div class="entry-content">
				<?php the_content(); ?>
				<select class="filter list-specialties">
					<option value="all">Filter by specialty</option>
					<option value="all" data-specialty="women">Women's Issues</option>
					<option value="all" data-specialty="mens">Men's Issues</option>
					<option value="all" data-specialty="gender">Gender Identity</option>
					<option value="all" data-specialty="gay">Gay/Lesbian Issues</option>
					<option value="all" data-specialty="relationships">Relationships</option>
					<option value="all" data-specialty="trauma">Trauma</option>
					<option value="all" data-specialty="grief">Grief/Loss</option>
					<option value="all" data-specialty="adoption">Adoption/Infertility</option>
					<option value="all" data-specialty="mood">Modd Disorders</option>
					<option value="all" data-specialty="dissociative">Dissociative Disorders</option>
					<option value="all" data-specialty="personality">Personality Disorders</option>
					<option value="all" data-specialty="add">Attention Deficit/Hyperactivity</option>
					<option value="all" data-specialty="eating">Eating Disorders</option>
					<option value="all" data-specialty="addiction">Addiction/Substance Abuse</option>
					<option value="all" data-specialty="aging">Aging and Geriatric Services</option>
					<option value="all" data-specialty="other">Other</option>
				</select>
				<input type="text" class="form-control input-lg" id="search" placeholder="search by name or location...">
				<?php get_template_part('template-parts/content', 'find-therapist'); ?>
			</div>
			
		</div>
		
	</div><!-- /#content -->

<?php endwhile; endif; ?>


	
<?php get_sidebar(); ?>
<?php get_footer(); ?>
