jQuery(document).ready(function($) {

/* Begin Dropdown Menu Scripts */

	// Adds arrow to li with sub-menu
	$('#menu-sidebar li').has('.sub-menu').addClass('with-menu');

	// IE Version Sniffer
	function getIEVersion() {
		var rv = -1;
		if (navigator.appName == 'Microsoft Internet Explorer') {
			var ua = navigator.userAgent;
			var re = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
			if (re.exec(ua) != null) {
				rv = parseFloat( RegExp.$1 );
			}
		}
		return rv;
	}

	// Sets special minWidth for Superfish if IE7
	var minW = getIEVersion() == 7 ? 19 : 12;

	// Superfish and Supersubs Drop-down Menus
	$('#menu-sidebar .sub-menu').show();
	$('#menu-sidebar').supersubs({
			minWidth:    minW,	// minimum width of sub-menus in em units
			maxWidth:    30,	// maximum width of sub-menus in em units
			extraWidth:  1		// extra width can ensure lines don't sometimes turn over
								// due to slight rounding differences and font-family
		})
		.superfish({
			animation: {opacity:'show',height:'show'},
			autoArrows: false,
			delay: 0
	});

/* End Dropdown Menu Scripts */

	// Event handlers for newsletter subscribe form
	$('.newsletter-signup').focus(function() {
		var $this = $(this);
		if ($this.val() === $this.attr('title')) {
			$this.val('');
		} else {
			$this.select();
		}
	});

	$('.newsletter-signup').blur(function() {
		var $this = $(this);
		if ( $this.val() === '') {
			$this.val($this.attr('title'));
		}
	});

});
