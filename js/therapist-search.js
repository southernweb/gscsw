jQuery(document).ready(function(){

        jQuery("select.filter").change(function(){
                var filters = jQuery(this).find(':selected').data('specialty');
                console.log(filters);


            if( jQuery(".member-row[data-specialty*='"+filters+"']").length == 0){
                
                jQuery( '.no-results-found').show();
                jQuery(".member-row:not([data-specialty*='"+filters+"'])").hide();
            } else {
                
                jQuery(".no-results-found").hide();
                jQuery(".member-row:not([data-specialty*='"+filters+"'])").hide();
                jQuery(".member-row[data-specialty*='"+filters+"']").show();
            }
        });


        jQuery('#search').on("keyup",function(e){

            var searchField = jQuery('#search').val();
            var searchData = searchField.toLowerCase();

            var searchtest = jQuery(".member-row .unit-50[data-name*='"+searchData+"']").length;

            console.log(searchtest);

            if(searchData == ""){
                jQuery(".no-results-found").hide();
            } else if(jQuery(".member-row[data-name*='"+searchData+"']").length == 0){
                jQuery("h1.letter").css("display", "none");
                jQuery(".member-row:not([data-name*='"+searchData+"'])").css("display","none");
                jQuery(".no-results-found").show();
            } else if(jQuery(".member-row[data-name*='"+searchData+"']").length > 0){
                jQuery(".no-results-found").hide();
                jQuery("h1.letter").css("display", "none");
                jQuery(".member-row:not([data-name*='"+searchData+"'])").css("display","none");
                jQuery(".member-row[data-name*='"+searchData+"']").show();
                
            } 

        });


});