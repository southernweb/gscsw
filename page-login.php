<?php 

if ( is_user_logged_in() ) {

	get_currentuserinfo();
    $user_info = get_userdata($current_user->ID);

	if( in_array('administrator', $user_info->roles) ) {
		wp_redirect( '/wp-admin/' );
		exit;
	} elseif (in_array('member', $user_info->roles)) {
		wp_redirect( '/edit-profile/' );
		exit;
	}
	elseif (in_array('inactive', $user_info->roles)) {
		wp_redirect( '/account-renewal/' );
		exit;
	}
}


get_header(); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		
	<div id="content" class="clearfix">
	
		<div <?php post_class(); ?>>
		
			<span class="entry-title hide"><?php the_title();?></span>
			
			<div class="entry-content">
				<?php get_template_part( 'template-parts/content', 'login' ); ?>
			</div>
			
		</div>
		
	</div><!-- /#content -->

<?php endwhile; endif; ?>


	
<?php get_sidebar(); ?>
<?php get_footer(); ?>