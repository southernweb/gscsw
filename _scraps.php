---------------------------------------------------------------------------
---------------------------------------------------------------------------
HEADER.PHP
---------------------------------------------------------------------------
---------------------------------------------------------------------------

|| ---------------------------------------------------- ||
|| ***** For Targeting IE (Must Update .htaccess) ***** ||
|| ---------------------------------------------------- ||

<!--[if lt IE 7 ]> <html class="no-js ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie7" lang="en"><![endif]-->
<!--[if IE 8 ]>    <html class="no-js ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--> <html lang="en"> <!--<![endif]-->

|| ------------------------- ||
|| ***** Favicon *********** ||
|| ------------------------- ||

	<link rel="shortcut icon" href="<?php bloginfo('template_directory'); ?>/ui/favicon.ico" type="image/x-icon" />

---------------------------------------------------------------------------
---------------------------------------------------------------------------
STYLE.CSS
---------------------------------------------------------------------------
---------------------------------------------------------------------------

<style type="text/css">

/* ----- Button Styles ( to go with button shortcode ) ----- */

.button a {
	background: #d3d3d3;
	-moz-border-radius: 3px;
	-webkit-border-radius: 3px;
	border-radius: 3px;
	color: #000;
	display: block;
	float: left;
	padding: 5px 20px;
	text-decoration: none;
}

	.button a:hover {
		background: #e3e3e3;
	}

.right a {
	float: right;
}


/* ----- Blog ----- */

.post-meta {
	color: #999;
	font-size: .875em;
}

.post-nav { }

	.next-posts {
		float: left;
	}

	.prev-posts {
		float: right;
	}

/* ----- Comments ----- */

#content ol.commentlist {
	margin: 0;
}

	#content ol.commentlist li {
		background: #f6f6f6;
		border: 1px solid #dadada;
		list-style: none;
		padding: 15px;
		margin: 10px 0;
	}

		ol.commentlist ul.children {
			margin: 6px 0;
		}

			ol.commentlist ul.children li {
				border: solid 1px #d3d3d3;
				border-left-width: 3px;
				margin: 6px 0;
			}

		ol.commentlist div.comment-author {
			font-weight: bold;
		}

		.commentlist .reply {
			text-align: right;
		}

#respond {
	background: #e9e9e9;
	border: 1px solid #dadada;
	padding: 18px;
	margin-top: 20px;
}

#respond h2 {
	line-height: 1.5em;
	margin: 0 0 8px 0;
	padding: 0 0 0 0;
	font-size: 20px;
}

#respond div {
	margin: 8px 0;
}

#respond label {
	display: block;
	padding: 0 0 3px 0;
}

#respond input,
#respond textarea {
	background: #fff;
	border: 1px solid #c6c6c5;
	-webkit-border-radius: 3px;
	-moz-border-radius: 3px;
	border-radius: 3px;
	padding: 4px 4px;
}

	#respond input[type=submit] {
		background: #222;
		background: -moz-linear-gradient(top, #333, #000);
		background: -webkit-gradient(linear, left top, left bottom, from(#333), to(#000));
		border: solid 1px #000;
		-moz-box-shadow: inset 0 0 2px rgba(255,255,255,0.8);
		-webkit-box-shadow: inset 0 0 2px rgba(255,255,255,0.8);
		box-shadow: inset 0 0 2px rgba(255,255,255,0.8);
		color: #FFF;
		cursor: pointer;
		margin-top: 5px;
		padding: 10px;
		text-transform: uppercase;
	}

	#respond input[type=submit]:hover {
		background: #444;
		background: -moz-linear-gradient(top, #555, #222);
		background: -webkit-gradient(linear, left top, left bottom, from(#555), to(#222));
	}

</style>

---------------------------------------------------------------------------
---------------------------------------------------------------------------
FUNCTIONS.PHP
---------------------------------------------------------------------------
---------------------------------------------------------------------------

<?php

// Widget Regions
if (function_exists('register_sidebar')) {
	register_sidebar(array(
		'name' => 'Sidebar Widgets',
		'id'   => 'sidebar-widgets',
		'description'   => 'These are widgets for the sidebar.',
		'before_widget' => '<li id="%1$s" class="widget %2$s">',
		'after_widget'  => '</li>',
		'before_title'  => '<h3>',
		'after_title'   => '</h3>'
	));
	register_sidebar(array(
		'name' => 'Footer Widgets',
		'id'   => 'footer-widgets',
		'description'   => 'These are widgets for the footer.',
		'before_widget' => '<li id="%1$s" class="widget %2$s">',
		'after_widget'  => '</li>',
		'before_title'  => '<h3>',
		'after_title'   => '</h3>'
	));
}

// Excerpts

function excerpt_read_more_link($output) {
 global $post;
 return $output . '<br/><a href="'. get_permalink($post->ID) . '"> Read More &raquo;</a>';
}
add_filter('the_excerpt', 'excerpt_read_more_link');

function new_excerpt_more($excerpt) {
	return str_replace('[...]', '...', $excerpt);
}
add_filter('wp_trim_excerpt', 'new_excerpt_more');

function custom_excerpt_length( $length ) {
	return 25;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );


// Checks if page is the page with the given ID or one of its descendants
function is_tree($pid) {
	global $post;

	if ( is_page($pid) ) {
		return true;
	}

	$anc = get_post_ancestors( $post->ID );
	foreach ( $anc as $ancestor ) {
		if( is_page() && $ancestor == $pid ) {
			return true;
		}
	}
    return false;
}

// Checks if page is direct child of the page with the given ID
function is_child( $parent ) {
	global $post;
	if( is_array($parent) ) {
		return in_array( $post->post_parent, $parent );
	} else {
		return $post->post_parent == $parent;
	}
}


// Checks if page is a grandchild of the page with the given ID
function is_grandchild($gpid) {
	global $post;
	if( is_tree($gpid) && !is_child($gpid) && !is_page($gpid) ) {
		return true;
	} else {
		return false;
	}
}

// Adds image size
if ( function_exists( 'add_image_size' ) ) {
	add_image_size('my_thumbnail', 200, 9999);
}

// subpages shortcode
function subpage_shortcode($atts) {

	global $post;
	extract( shortcode_atts( array(
		'child_of' => $post->ID
	), $atts ) );

	$attributes = shortcode_atts(array('title_li' => '', 'echo' => 0, 'depth' => 1, 'child_of' => $child_of));

	return '<ul class="sub-pages sub-pages-of-' . $post->ID . '">' . wp_list_pages($attributes) . '</ul>';

}
add_shortcode('subpages', 'subpage_shortcode');


// [children] Shortcode Handler
function children_shortcode($attributes)
{
	$attributes = shortcode_atts(array('title_li' => '', 'echo' => 0), $attributes);

	if (!isset($attributes['child_of'])) {
		global $post;
		if (is_object($post) && isset($post->ID)) {
			$attributes['child_of'] = $post->ID;
		}
	}

	return '<ul>' . wp_list_pages($attributes) . '</ul>';
}

//	Columns
function basic_column($atts, $content = null) {
	return '<div class="col">' . $content . '</div>';
}
add_shortcode("col", "basic_column");


//	Clear
function basic_clear($atts, $content = null) {
	return '<div class="clear"></div>';
}
add_shortcode("clear", "basic_clear");


// WooCommerce Editor additons for management
$role_object->add_cap( 'manage_woocommerce' );
$role_object->add_cap( 'manage_woocommerce_orders' );
$role_object->add_cap( 'manage_woocommerce_coupons' );
$role_object->add_cap( 'manage_woocommerce_products' );
$role_object->add_cap( 'view_woocommerce_reports' );



// custom post type
add_action( 'init', 'create_post_type' );
function create_post_type() {
	register_post_type( 'testimonial',
		array(
			'labels' => array(
				'name' => __('Testimonials'),
				'singular_name' => __('Testimonial'),
				'add_new' => __( 'Add New' ),
				'add_new_item' => __( 'Add New Testimonial' ),
				'edit' => __( 'Edit' ),
				'edit_item' => __( 'Edit Testimonial' ),
				'new_item' => __( 'New Testimonial' ),
				'view' => __( 'View Testimonial' ),
				'view_item' => __( 'View Testimonial' ),
				'search_items' => __( 'Search Testimonials' ),
				'not_found' => __( 'No testimonials found' ),
				'not_found_in_trash' => __( 'No testimonials found in Trash' ),
				'parent' => __( 'Parent Testimonial' )
			),
			'public' => true,
			'supports' => array( 'title', 'editor' ),
			'rewrite' => array( 'slug' => 'testimonial', 'with_front' => false)
		)
	);
}

// custom taxonomy
add_action( 'init', 'create_custom_taxonomies' );
function create_custom_taxonomies() {

	register_taxonomy( 'faq_cat', array('faq'),
		array(
		    'hierarchical' => true,
		    'labels' => array(
			    'name' => _x( 'FAQ Categories', 'taxonomy general name' ),
			    'singular_name' => _x( 'FAQ Category', 'taxonomy singular name' ),
			    'search_items' =>  __( 'Search FAQ Categories' ),
			    'all_items' => __( 'All FAQ Categories' ),
			    'parent_item' => __( 'Parent FAQ Category' ),
			    'parent_item_colon' => __( 'Parent FAQ Category:' ),
			    'edit_item' => __( 'Edit FAQ Category' ),
			    'update_item' => __( 'Update FAQ Category' ),
			    'add_new_item' => __( 'Add New FAQ Category' ),
			    'new_item_name' => __( 'New FAQ Category Name' ),
			    'menu_name' => __( 'FAQ Categories' ),
			),
		    'show_ui' => true,
		    'query_var' => true,
		    'rewrite' => array( 'slug' => 'faq_cat' ),
		)
	);

}

// Button Shortcode
function button_shortcode( $atts ) {
	extract( shortcode_atts( array(
		'text' => 'Read more',
		'url' => '/',
		'align' => 'right'
	), $atts ) );

	return '<p class="clearfix button ' . $align . '"><a href="' . $url . '">' . $text . '</a></p>';
}
add_shortcode( 'button', 'button_shortcode' );

// Remove menu items from client user
function remove_menus() {
	global $menu;
	global $current_user;
	get_currentuserinfo();
	if($current_user->user_login != 'admin@swg') {
		$restricted = array(
			//__('Dashboard'),
			//__('Posts'),
			//__('Media'),
			__('Links'),
			//__('Pages'),
			//__('Appearance'),
			__('Tools'),
			//__('Users'),
			__('Settings'),
			__('Comments')
			//__('Plugins')
		);
		end ($menu);
		while (prev($menu)){
			$value = explode(' ',$menu[key($menu)][0]);
			if(in_array($value[0] != NULL?$value[0]:"" , $restricted)){unset($menu[key($menu)]);}
		}
	}
}
add_action('admin_menu', 'remove_menus');

// Remove Google Analyticator from menu
global $current_user;
get_currentuserinfo();
if($current_user->user_login != 'admin@swg') {
	remove_action('admin_menu', 'add_ga_option_page');
}

/* http://wordpress.stackexchange.com/questions/2802/display-a-portion-branch-of-the-menu-tree-using-wp-nav-menu  */

class Selective_Walker extends Walker_Nav_Menu
{
    function walk( $elements, $max_depth) {

        $args = array_slice(func_get_args(), 2);
        $output = '';

        if ($max_depth < -1) //invalid parameter
            return $output;

        if (empty($elements)) //nothing to walk
            return $output;

        $id_field = $this->db_fields['id'];
        $parent_field = $this->db_fields['parent'];

        // flat display
        if ( -1 == $max_depth ) {
            $empty_array = array();
            foreach ( $elements as $e )
                $this->display_element( $e, $empty_array, 1, 0, $args, $output );
            return $output;
        }

        /*
         * need to display in hierarchical order
         * separate elements into two buckets: top level and children elements
         * children_elements is two dimensional array, eg.
         * children_elements[10][] contains all sub-elements whose parent is 10.
         */
        $top_level_elements = array();
        $children_elements  = array();
        foreach ( $elements as $e) {
            if ( 0 == $e->$parent_field )
                $top_level_elements[] = $e;
            else
                $children_elements[ $e->$parent_field ][] = $e;
        }

        /*
         * when none of the elements is top level
         * assume the first one must be root of the sub elements
         */
        if ( empty($top_level_elements) ) {

            $first = array_slice( $elements, 0, 1 );
            $root = $first[0];

            $top_level_elements = array();
            $children_elements  = array();
            foreach ( $elements as $e) {
                if ( $root->$parent_field == $e->$parent_field )
                    $top_level_elements[] = $e;
                else
                    $children_elements[ $e->$parent_field ][] = $e;
            }
        }

        $current_element_markers = array( 'current-menu-item', 'current-menu-parent', 'current-menu-ancestor' );  //added by continent7
        foreach ( $top_level_elements as $e ){  //changed by continent7
            // descend only on current tree
            $descend_test = array_intersect( $current_element_markers, $e->classes );
            if ( !empty( $descend_test ) )
                $this->display_element( $e, $children_elements, 3, 0, $args, $output );
        }

        /*
         * if we are displaying all levels, and remaining children_elements is not empty,
         * then we got orphans, which should be displayed regardless
         */
         /* removed by continent7
        if ( ( $max_depth == 0 ) && count( $children_elements ) > 0 ) {
            $empty_array = array();
            foreach ( $children_elements as $orphans )
                foreach( $orphans as $op )
                    $this->display_element( $op, $empty_array, 1, 0, $args, $output );
         }
        */
         return $output;
    }
}

?>


Example usage of above Walker class
Requires page to have children or be a sub-page and have siblings

<?php
	$children = get_pages('child_of=' . $post->ID);
	$siblings = get_pages('child_of=' . $post->post_parent);
	if( count($children) > 0 || ( $post->post_parent && count($siblings) > 0 ) ) {
?>

	<div class="sidebar-entry">

		<?php
			wp_nav_menu( array(
				'menu' => 'Main',
				'container' => false,
				'depth' => 0,
				'walker'=>new Selective_Walker()
			));
	   ?>

	</div>

<?php } ?>


<?php

/**
*
*	Dynamic Include
*
*	Save the content of an include file to a variable
*
*	example: $callout = dynamic_include('inc/callout.php');
*
*/

function dynamic_include($file) {

	ob_start();
	include($file);
	$file_contents = ob_get_contents();
	ob_end_clean();

	return $file_contents;

}

// Example Usage

$content = get_the_content();
$content = apply_filters('the_content', $content);
$content = str_replace(']]>', ']]&gt;', $content);

$buttons = dynamic_include('inc/buttons.php');

$content = preg_replace('/((<p>)?\s*\[buttons\]\s*(<\/p>)?)/', $buttons, $content);

echo $content;



/*
*********************************************
Function to allow multi-line photo captions.
This function will split captions onto multiple lines if it detects
a "|" (pipe) symbol.
**********************************************
*/
/* Override existing caption shortcode handlers with our own */
add_shortcode('wp_caption', 'multiline_caption');
add_shortcode('caption', 'multiline_caption');

/* Our new function */
function multiline_caption($attr, $content = null) {
	extract(shortcode_atts(array(
	'id' => '',
	'align' => 'alignnone',
	'width' => '',
	'caption' => ''
	), $attr));

	if ( 1 > (int) $width || empty($caption) )
	return $content;

	if ( $id ) $id = 'id="' . esc_attr($id) . '" ';

	$new_caption = str_replace("|", "<br />", $caption);

	return '<div ' . $id . 'class="wp-caption ' . esc_attr($align) . '" style="width: ' . (10 + (int) $width) . 'px">'
	. do_shortcode( $content ) . '<p class="wp-caption-text">' . $new_caption . '</p></div>';
}

/* ********************************************* */

?>

<?php if( is_front_page() ) { ?>
	<div id="slider">
		<?php while(the_repeater_field('slider')) : ?>
			<a class="slide" href="<?php the_sub_field('slide_link'); ?>">
				<img src="<?php the_sub_field('slide_image'); ?>" width="920" height="300" alt="" />
			</a>
		<?php endwhile; ?>
	</div>

	<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/jquery.cycle.lite.min.js"></script>
	<script type="text/javascript">
		jQuery(document).ready(function($) {
			$('#slider').cycle({
				speed: 1500,
				timeout: 5000
			});
		});
	</script>
<?php } ?>
