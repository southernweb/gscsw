<?php get_header(); ?>

<div id="content" class="hentry">

	<h2 class="entry-title">Page Not Found</h2>
	
	<div class="entry-content">
	
		<p>We're very sorry, but the page you are looking for doesn't seem to exist. Please try using the navigation above.</p>
	
	</div>
	
</div><!-- /#content -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>