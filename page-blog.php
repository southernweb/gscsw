<?php 
/*
*	Template Name: Blog
*/
get_header(); 
?>
			
<div id="content">
	
	<?php 
		if (have_posts()) : while (have_posts()) : the_post();
			the_content();
		endwhile; endif;
		wp_reset_query();
	?>
	
	<?php 
		$post_query = new WP_Query(array(
			'posts_per_page' => 5,
			'paged' => $paged
		));
		if( $post_query->have_posts() ) : 
	?>
	
		<?php while ( $post_query->have_posts() ) : $post_query->the_post(); ?>
	
			<div <?php post_class() ?>>
	
				<hr />
			
				<h2 class="entry-title"><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h2>
				
				<?php get_template_part('inc/post', 'before-meta'); ?>
				
				<div class="entry-summary">
					<?php the_excerpt(); ?>
				</div>
				
				<?php get_template_part('inc/post', 'after-meta'); ?>
				
			</div><!-- /.entry -->
	
		<?php endwhile; ?>

		<?php get_template_part('inc/post', 'nav'); ?>
	
	<?php endif; ?>
	
</div><!-- /#content -->
	
<?php get_sidebar(); ?>
<?php get_footer(); ?>