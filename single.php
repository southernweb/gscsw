<?php get_header(); ?>

<div id="content" class="clearfix">

	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

		<div <?php post_class() ?>>
			
			<h1 class="entry-title"><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h1>
			
			<?php get_template_part('inc/post', 'before-meta'); ?>
				
			<?php the_content(); ?>

			<?php get_template_part('inc/post', 'after-meta'); ?>
			
		</div>

		<?php //comments_template(); ?>

	<?php endwhile; endif; ?>

</div><!-- /#content -->
	
<?php get_sidebar(); ?>
<?php get_footer(); ?>