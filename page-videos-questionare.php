<?php 

/* Template Name: Video Questionaire*/

get_header(); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		
	<div id="content" class="clearfix">
	
		<div <?php post_class(); ?>>
		
			<span class="entry-title hide"><?php the_title();?></span>
			
			<div class="entry-content videos-page">

			    <?php $user = wp_get_current_user(); ?>
				<?php if ( is_user_logged_in() ) { 
					if( in_array( 'member', (array) $user->roles ) || in_array( 'administrator', (array) $user->roles )) { 
						the_content();
					} elseif (in_array( 'inactive', (array) $user->roles )) {?>
						 <h1>Videos Questionare</h1>
						 <?php the_field('message'); ?>
					<?php } 
				} else { ?>
					<div class="login-message">
						 <h1>Videos Questionare</h1>
						 <?php the_field('message'); ?>
					</div>
				<?php } ?>

			</div>
			
		</div>
		
	</div><!-- /#content -->

<?php endwhile; endif; ?>


	
<?php get_sidebar(); ?>
<?php get_footer(); ?>