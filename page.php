<?php get_header(); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		
	<div id="content" class="clearfix">
	
		<div <?php post_class(); ?>>
		
			<span class="entry-title hide"><?php the_title();?></span>
			
			<div class="entry-content">
				<?php the_content(); ?>
			</div>
			
		</div>
		
	</div><!-- /#content -->

<?php endwhile; endif; ?>


	
<?php get_sidebar(); ?>
<?php get_footer(); ?>