<div class="login-form">
		<h1 class="entry-title">Login</h1>
	<?php 
		// if login fails, show error message.
		if(isset($_GET['login']) && $_GET['login'] == 'failed') {
			?>

			<p>
				<strong>Please login below before making any changes to your membership</strong>
			</p>

			<?php
				$args = array( 
					'label_username' => __( 'Username' ),
					'label_password' => __( 'Password' ),
					'label_log_in'   => __( 'Enter' ),
					'remember'		 => false
				); 	

				// Show login form if not logged in or if login has not failed.
				wp_login_form($args); ?>

				<div id="login-error" style="background-color: #FFEBE8;border:1px solid #C00;padding:5px;">
					<p>Login failed: You have entered an incorrect Username or password, please try again.</p>
				</div>
				<div class="login-forgot-password">
					<a href="<?php echo wp_lostpassword_url(); ?>">Forgot Password?</a> 
				</div>


			<?php
		}
		// Conditional if user is logged in 
		elseif ( is_user_logged_in() ) {

			// hide the login form
			wp_login_form( array( 'echo' => false ) );

			// retrieve logged in user info 
			global $current_user;
		?>
			<div class="login-title">login</div>
			<!-- Display user "display name" -->
			<div style="color: #000;">Welcome <?php echo $current_user->display_name; ?>!</div>
			<a href="<?php echo wp_logout_url(); ?>">logout</a>
		<?php } 
		else { ?>

			<p>
				<strong>Please login below before making any changes to your membership</strong>
			</p>

			<?php
				$args = array( 
					'label_username' => __( 'Username' ),
					'label_password' => __( 'Password' ),
					'label_log_in'   => __( 'Enter' ),
					'remember'		 => false
				); 	

				// Show login form if not logged in or if login has not failed.
				wp_login_form($args); ?>

				<hr/>
				<p>
					Please login using your email address and password.<br>
					If you can't remember your password please <a href="<?php echo wp_lostpassword_url(); ?>">click here</a>.
				</p>
			<?php }
		?>
</div>