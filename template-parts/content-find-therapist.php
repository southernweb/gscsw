<?php 

$args = array(
	'role'         => 'member',
	'orderby'      => 'last_name',
	'order'        => 'ASC'
 ); ?>


<?php  // Get all users order by amount of posts

$allUsers = get_users($args);

$users = array();

?>

<div class="member-head">
	<div class="member-50">First Name</div>
	<div class="member-50">Last Name</div>
</div>

<?php 
	// Sort all user by last name
	usort($allUsers, create_function('$a, $b', 'return strnatcasecmp($a->last_name, $b->last_name);')); 
?>
<?php foreach($allUsers as $user) { 

	$firstNameLower = strtolower($user->first_name);
	$lastNameLower = strtolower($user->last_name);
	$city = strtolower($user->office_city);
	$practiceShort = strtolower($user->practice_short);

	$p_women = get_the_author_meta( 'practice_women_issues', $user->ID);
	$p_men = get_the_author_meta( 'practice_men_issues', $user->ID);
	$p_gender = get_the_author_meta( 'practice_gender_identity', $user->ID);
	$p_gay = get_the_author_meta( 'practice_gay_issues', $user->ID);
	$p_relationships = get_the_author_meta( 'practice_relationships', $user->ID);
	$p_trauma = get_the_author_meta( 'practice_trauma', $user->ID);
	$p_grief = get_the_author_meta( 'practice_grief', $user->ID);
	$p_adoption = get_the_author_meta( 'practice_adoption', $user->ID);
	$p_mood = get_the_author_meta( 'practice_mood', $user->ID);
	$p_dissociative = get_the_author_meta( 'practice_dissociative', $user->ID);
	$p_personality = get_the_author_meta( 'practice_personality', $user->ID);
	$p_add = get_the_author_meta( 'practice_add', $user->ID);
	$p_eating = get_the_author_meta( 'practice_eating', $user->ID);
	$p_addiction = get_the_author_meta( 'practice_addiction', $user->ID);
	$p_aging = get_the_author_meta( 'practice_aging', $user->ID);
	$p_other = get_the_author_meta( 'practice_other', $user->ID);


	?>


	<?php $display_on_directory = strtolower (get_the_author_meta('display_on_directory', $user->ID)); ?>

	<?php if($display_on_directory == 'yes'){ ?>
		<div class="member-row" 
			 data-name="<?php echo $firstNameLower. ' ' . $lastNameLower . ' ' . $city ?>" 
			 data-specialty="
			 	<?php if($p_women){ ?>women <?php } ?>
				<?php if($p_men){ ?>mens <?php } ?>
				<?php if($p_gender){ ?>gender <?php } ?>
				<?php if($p_gay){ ?>gay <?php } ?>
				<?php if($p_relationships){ ?> relationships <?php } ?>
				<?php if($p_trauma){ ?>trauma <?php } ?>
				<?php if($p_grief){ ?>grief <?php } ?>
				<?php if($p_adoption){ ?>adoption <?php } ?>
				<?php if($p_mood){ ?>mood <?php } ?>
				<?php if($p_dissociative){ ?> dissociative <?php } ?>
				<?php if($p_personality){ ?>personality <?php } ?>
				<?php if($p_add){ ?>add <?php } ?>
				<?php if($p_eating){ ?>eating <?php } ?>
				<?php if($p_addiction){ ?>addiction <?php } ?>
				<?php if($p_aging){ ?>aging <?php } ?>
				<?php if($p_other){ ?>other <?php } ?>
			 "
		>

			<div class="member-50">
				<a href="<?php echo get_author_posts_url( $user->ID ); ?>"><?php echo $user->first_name; ?></a>
			</div>
			<div class="member-50">
				<a href="<?php echo get_author_posts_url( $user->ID ); ?>"><?php echo $user->last_name; ?></a>
			</div>
		</div>
	<?php } ?>
<?php } ?>
<div class="no-results-found">No results found.</div>