<?php 

$args = array(
	'role'         => 'member',
	'orderby'      => 'last_name',
	'order'        => 'ASC'
 ); ?>


<?php  // Get all users order by amount of posts

$allUsers = get_users($args);
$users = array();

?>

<div class="member-head">
	<div class="member-50">First Name</div>
	<div class="member-50">Last Name</div>
</div>

<?php 
	// Sort all user by last name
	usort($allUsers, create_function('$a, $b', 'return strnatcasecmp($a->last_name, $b->last_name);')); 
?>
<?php foreach($allUsers as $user) { ?>

<?php $display_on_directory = strtolower (get_the_author_meta('display_on_directory', $user->ID)); ?>

<?php if($display_on_directory == 'yes'){ ?>
<div class="member-row">
	<div class="member-50">
		<a href="<?php echo get_author_posts_url( $user->ID ); ?>"><?php echo $user->first_name; ?></a>
	</div>
	<div class="member-50">
		<a href="<?php echo get_author_posts_url( $user->ID ); ?>"><?php echo $user->last_name; ?></a>
	</div>
</div>
<?php } ?>
<?php } ?>