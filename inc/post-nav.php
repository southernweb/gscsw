<div class="post-nav clearfix">

	<div class="next-posts"><?php next_posts_link('&laquo; Older Entries', $post_query->max_num_pages) ?></div>
	
	<div class="prev-posts"><?php previous_posts_link('Newer Entries &raquo;') ?></div>
	
</div>