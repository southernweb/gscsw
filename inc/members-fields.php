<?php

// show custom meta fields on EAP Member accounts
add_action( 'show_user_profile', 'my_show_extra_profile_fields' );
add_action( 'edit_user_profile', 'my_show_extra_profile_fields' );

function my_show_extra_profile_fields( $user ) { ?>

	<?php if ($user->has_cap('member') || $user->has_cap('inactive')) { ?>


	<h3>Basic information</h3>
	<!-- <table class="form-table">
		<tr>
			<th><label for="renew-date">Renewal Date</label></th>
			<td>
				<input type="text" name="renew-date" id="renew-date" value="<?php echo esc_attr( get_the_author_meta( 'renew_date', $user->ID ) ); ?>" class="regular-text" /><br />
			</td>
		</tr>
	</table> -->
	<table class="form-table">
		<!-- <tr>
			<th><label for="hide-email">Registration Date</label></th>

			<td>
				<?php
				// $registeredDate = get_the_author_meta( 'user_registered', $user->ID );
				// $registeredDateFormatted =  date("d/m/y", strtotime($registeredDate));
				// echo $registeredDateFormatted;

	            ?>

			</td>
		</tr> -->
		<tr>
			<th><label for="hide-email">Hide Email From Public View</label></th>

			<td>
				<?php
	            $selected = get_the_author_meta( 'hide_email', $user->ID );
	            ?>
	            <select name="hide-email" id="hide-email">
	                <option value="Yes" <?php echo ($selected == "Yes")?  'selected="selected"' : '' ?>>Yes</option>
	                <option value="No" <?php echo ($selected == "No")?  'selected="selected"' : '' ?>>No</option>
				</select>
			</td>
		</tr>
		<tr>
			<th><label for="accept-insurance">Do you Accept Insurance?</label></th>
			<td>
				<?php
	            $selected = get_the_author_meta( 'accept_insurance', $user->ID );
	            ?>
	            <select name="accept-insurance" id="accept-insurance">
	                <option value="Yes" <?php echo ($selected == "Yes")?  'selected="selected"' : '' ?>>Yes</option>
	                <option value="No" <?php echo ($selected == "No")?  'selected="selected"' : '' ?>>No</option>
				</select>
			</td>
		</tr>
		<tr>
			<th><label for="communication-preference">Communication Preference:</label></th>
			<td>
				<?php
	            $selected = get_the_author_meta( 'communication_preference', $user->ID );
	            ?>
	            <select name="communication-preference" id="communication-preference">
	                <option value="Email" <?php echo ($selected == "Email")?  'selected="selected"' : '' ?>>Email</option>
	                <option value="US Mail" <?php echo ($selected == "US Mail")?  'selected="selected"' : '' ?>>US Mail</option>
				</select>
			</td>
		</tr>
		<tr>
			<th><label for="mail-location">Mail Location Preference:</label></th>
			<td>
				<?php
	            $selected = get_the_author_meta( 'mail_location', $user->ID );
	            ?>
	            <select name="mail-location" id="mail-location">
	                <option value="Home" <?php echo ($selected == "Home")?  'selected="selected"' : '' ?>>Home</option>
	                <option value="Office" <?php echo ($selected == "Office")?  'selected="selected"' : '' ?>>Office</option>
				</select>
			</td>
		</tr>
		<tr>
			<th><label for="are-supervisor">Are you able to be a Supervisor?</label></th>
			<td>
				<?php
	            $selected = get_the_author_meta( 'are_supervisor', $user->ID );
	            ?>
	            <select name="are-supervisor" id="are-supervisor">
	           		<option value="No" <?php echo ($selected == "No")?  'selected="selected"' : '' ?>>No</option>
	                <option value="Yes" <?php echo ($selected == "Yes")?  'selected="selected"' : '' ?>>Yes</option>
				</select>
			</td>
		</tr>
		<tr>
			<th><label for="display-on-directory">Would you like your information displayed in our online directory?</label></th>
			<td>
				<?php
	            $selected = get_the_author_meta( 'display_on_directory', $user->ID );
	            ?>
	            <select name="display-on-directory" id="display-on-directory">
	                <option value="Yes" <?php echo ($selected == "Yes")?  'selected="selected"' : '' ?>>Yes</option>
	                <option value="No" <?php echo ($selected == "No")?  'selected="selected"' : '' ?>>No</option>
				</select>
			</td>
		</tr>
	</table>


	<h3>Home information</h3>
	<table class="form-table">
		<tr>
			<th><label for="home-address">Address</label></th>
			<td>
				<input type="text" name="home-address" id="home-address" value="<?php echo esc_attr( get_the_author_meta( 'home_address', $user->ID ) ); ?>" class="regular-text" /><br />
			</td>
		</tr>
		<tr>
			<th><label for="home-address-two">Address – Second Line</label></th>
			<td>
				<input type="text" name="home-address-two" id="home-address-two" value="<?php echo esc_attr( get_the_author_meta( 'home_address_two', $user->ID ) ); ?>" class="regular-text" /><br />
			</td>
		</tr>
		<tr>
			<th><label for="home-city">City</label></th>
			<td>
				<input type="text" name="home-city" id="home-city" value="<?php echo esc_attr( get_the_author_meta( 'home_city', $user->ID ) ); ?>" class="regular-text" /><br />
			</td>
		</tr>
		<tr>
			<th><label for="home-state">State</label></th>
			<td>
				<input type="text" name="home-state" id="home-state" value="<?php echo esc_attr( get_the_author_meta( 'home_state', $user->ID ) ); ?>" class="regular-text" /><br />
			</td>
		</tr>
		<tr>
			<th><label for="home-zip">Zip</label></th>
			<td>
				<input type="text" name="home-zip" id="home-zip" value="<?php echo esc_attr( get_the_author_meta( 'home_zip', $user->ID ) ); ?>" class="regular-text" /><br />
			</td>
		</tr>
		<tr>
			<th><label for="home-phone">Phone</label></th>
			<td>
				<input type="text" name="home-phone" id="home-phone" value="<?php echo esc_attr( get_the_author_meta( 'home_phone', $user->ID ) ); ?>" class="regular-text" /><br />
			</td>
		</tr>
		<tr>
			<th><label for="home-mobile-phone">Mobile Phone</label></th>
			<td>
				<input type="text" name="home-mobile-phone" id="home-mobile-phone" value="<?php echo esc_attr( get_the_author_meta( 'home_mobile_phone', $user->ID ) ); ?>" class="regular-text" /><br />
			</td>
		</tr>
	</table>


	<h3>Office Primary</h3>
	<table class="form-table">
		<tr>
			<th><label for="office-name">Office Name</label></th>
			<td>
				<input type="text" name="office-name" id="office-name" value="<?php echo esc_attr( get_the_author_meta( 'office_name', $user->ID ) ); ?>" class="regular-text" /><br />
			</td>
		</tr>
		<tr>
			<th><label for="office-address">Address</label></th>
			<td>
				<input type="text" name="office-address" id="office-address" value="<?php echo esc_attr( get_the_author_meta( 'office_address', $user->ID ) ); ?>" class="regular-text" /><br />
			</td>
		</tr>
		<tr>
			<th><label for="office-address-two">Address – Second Line</label></th>
			<td>
				<input type="text" name="office-address-two" id="office-address-two" value="<?php echo esc_attr( get_the_author_meta( 'office_address_two', $user->ID ) ); ?>" class="regular-text" /><br />
			</td>
		</tr>
		<tr>
			<th><label for="office-city">City</label></th>
			<td>
				<input type="text" name="office-city" id="office-city" value="<?php echo esc_attr( get_the_author_meta( 'office_city', $user->ID ) ); ?>" class="regular-text" /><br />
			</td>
		</tr>
		<tr>
			<th><label for="office-state">State</label></th>
			<td>
				<input type="text" name="office-state" id="office-state" value="<?php echo esc_attr( get_the_author_meta( 'office_state', $user->ID ) ); ?>" class="regular-text" /><br />
			</td>
		</tr>
		<tr>
			<th><label for="office-zip">Zip</label></th>
			<td>
				<input type="text" name="office-zip" id="office-zip" value="<?php echo esc_attr( get_the_author_meta( 'office_zip', $user->ID ) ); ?>" class="regular-text" /><br />
			</td>
		</tr>
		<tr>
			<th><label for="office-website">Website</label></th>
			<td>
				<input type="text" name="office-website" id="office-website" value="<?php echo esc_attr( get_the_author_meta( 'office_website_url', $user->ID ) ); ?>" class="regular-text" /><br />
			</td>
		</tr>
		<tr>
			<th><label for="office-phone">Phone</label></th>
			<td>
				<input type="text" name="office-phone" id="office-phone" value="<?php echo esc_attr( get_the_author_meta( 'office_phone', $user->ID ) ); ?>" class="regular-text" /><br />
			</td>
		</tr>
		<tr>
			<th><label for="home-fax">Fax</label></th>
			<td>
				<input type="text" name="home-fax" id="home-fax" value="<?php echo esc_attr( get_the_author_meta( 'home_fax', $user->ID ) ); ?>" class="regular-text" /><br />
			</td>
		</tr>
	</table>


	<h3>Office Secondary</h3>
	<table class="form-table">
		<tr>
			<th><label for="office-two-name">Office Name</label></th>
			<td>
				<input type="text" name="office-two-name" id="office-two-name" value="<?php echo esc_attr( get_the_author_meta( 'office_two_name', $user->ID ) ); ?>" class="regular-text" /><br />
			</td>
		</tr>
		<tr>
			<th><label for="office-two-address">Address</label></th>
			<td>
				<input type="text" name="office-two-address" id="office-two-address" value="<?php echo esc_attr( get_the_author_meta( 'office_two_address', $user->ID ) ); ?>" class="regular-text" /><br />
			</td>
		</tr>
		<tr>
			<th><label for="office-two-address-two">Address – Second Line</label></th>
			<td>
				<input type="text" name="office-two-address-two" id="office-two-address-two" value="<?php echo esc_attr( get_the_author_meta( 'office_two_address_two', $user->ID ) ); ?>" class="regular-text" /><br />
			</td>
		</tr>
		<tr>
			<th><label for="office-two-city">City</label></th>
			<td>
				<input type="text" name="office-two-city" id="office-two-city" value="<?php echo esc_attr( get_the_author_meta( 'office_two_city', $user->ID ) ); ?>" class="regular-text" /><br />
			</td>
		</tr>
		<tr>
			<th><label for="office-two-state">State</label></th>
			<td>
				<input type="text" name="office-two-state" id="office-two-state" value="<?php echo esc_attr( get_the_author_meta( 'office_two_state', $user->ID ) ); ?>" class="regular-text" /><br />
			</td>
		</tr>
		<tr>
			<th><label for="office-two-zip">Zip</label></th>
			<td>
				<input type="text" name="office-two-zip" id="office-two-zip" value="<?php echo esc_attr( get_the_author_meta( 'office_two_zip', $user->ID ) ); ?>" class="regular-text" /><br />
			</td>
		</tr>
		<tr>
			<th><label for="office-two-website">Website</label></th>
			<td>
				<input type="text" name="office-two-website" id="office-two-website" value="<?php echo esc_attr( get_the_author_meta( 'office_two_website_url', $user->ID ) ); ?>" class="regular-text" /><br />
			</td>
		</tr>
	</table>


	<h3>Practice Description</h3>
	<table class="form-table">
		<tr>
			<th><label for="practice-short">Short Description</label></th>
			<td>
				<input type="text" name="practice-short" id="practice-short" value="<?php echo esc_attr( get_the_author_meta( 'practice_short', $user->ID ) ); ?>" class="regular-text" /><br />
			</td>
		</tr>
		<tr>
			<th><label for="practice-long">Long Description</label></th>
			<td>
				<textarea rows="6" type="textarea" name="practice-long" class="regular-text"><?php echo esc_attr( get_the_author_meta( 'practice_long', $user->ID ) ); ?></textarea>
			</td>
		</tr>
	</table>

	<h3>Specialties</h3>
	<table class="form-table">
		<tr>
			<td>
				<?php if ( get_the_author_meta( 'practice_addiction', $user->ID ) ) { ?>
					<input id="practice-addiction" name="practice-addiction" type="checkbox" value="1" checked="checked" />
				<?php } else { ?>
					<input id="practice-addiction" name="practice-addiction" type="checkbox" value="1" <?php checked(esc_attr( get_the_author_meta( 'practice_addiction', $user->ID )), '1', true ); ?> />
				<?php } ?>
				<label class="description" for="practice-addiction">
					Addiction/Substance abuse
				</label>
			</td>
			<td>
				<?php if ( get_the_author_meta( 'practice_adoption', $user->ID ) ) { ?>
					<input id="practice-adoption" name="practice-adoption" type="checkbox" value="1" checked="checked" />
				<?php } else { ?>
					<input id="practice-adoption" name="practice-adoption" type="checkbox" value="1" <?php checked(esc_attr( get_the_author_meta( 'practice_adoption', $user->ID )), '1', true ); ?> />
				<?php } ?>
				<label class="description" for="practice-adoption">
					Adoption/infertility
				</label>
			</td>
			<td>
				<?php if ( get_the_author_meta( 'practice_aging', $user->ID ) ) { ?>
					<input id="practice-aging" name="practice-aging" type="checkbox" value="1" checked="checked" />
				<?php } else { ?>
					<input id="practice-aging" name="practice-aging" type="checkbox" value="1" <?php checked(esc_attr( get_the_author_meta( 'practice_aging', $user->ID )), '1', true ); ?> />
				<?php } ?>
				<label class="description" for="practice-aging">
					Aging and Geriatric Services
				</label>
			</td>
			<td>
				<?php if ( get_the_author_meta( 'practice_add', $user->ID ) ) { ?>
					<input id="practice-add" name="practice-add" type="checkbox" value="1" checked="checked" />
				<?php } else { ?>
					<input id="practice-add" name="practice-add" type="checkbox" value="1" <?php checked(esc_attr( get_the_author_meta( 'practice_add', $user->ID )), '1', true ); ?> />
				<?php } ?>
				<label class="description" for="practice-add">
					Attention deficit/Hyperactivity
				</label>
			</td>
		</tr>
		<tr>
			<td>
				<?php if ( get_the_author_meta( 'practice_dissociative', $user->ID ) ) { ?>
					<input id="practice-dissociative" name="practice-dissociative" type="checkbox" value="1" checked="checked" />
				<?php } else { ?>
					<input id="practice-dissociative" name="practice-dissociative" type="checkbox" value="1" <?php checked(esc_attr( get_the_author_meta( 'practice_dissociative', $user->ID )), '1', true ); ?> />
				<?php } ?>
				<label class="description" for="practice-dissociative">
					Dissociative Disorders
				</label>
			</td>
			<td>
				<?php if ( get_the_author_meta( 'practice_eating', $user->ID ) ) { ?>
					<input id="practice-eating" name="practice-eating" type="checkbox" value="1" checked="checked" />
				<?php } else { ?>
					<input id="practice-eating" name="practice-eating" type="checkbox" value="1" <?php checked(esc_attr( get_the_author_meta( 'practice_eating', $user->ID )), '1', true ); ?> />
				<?php } ?>
				<label class="description" for="practice-eating">
					Eating Disorders
				</label>
			</td>
			<td>
				<?php if ( get_the_author_meta( 'practice_gay_issues', $user->ID ) ) { ?>
					<input id="practice-gay-issues" name="practice-gay-issues" type="checkbox" value="1" checked="checked" />
				<?php } else { ?>
					<input id="practice-gay-issues" name="practice-gay-issues" type="checkbox" value="1" <?php checked(esc_attr( get_the_author_meta( 'practice_gay_issues', $user->ID )), '1', true ); ?> />
				<?php } ?>
				<label class="description" for="practice-gay-issues">
					Gay/Lesbian Issues
				</label>
			</td>
			<td>
				<?php if ( get_the_author_meta( 'practice_gender_identity', $user->ID ) ) { ?>
					<input id="practice-gender-identity" name="practice-gender-identity" type="checkbox" value="1" checked="checked" />
				<?php } else { ?>
					<input id="practice-gender-identity" name="practice-gender-identity" type="checkbox" value="1" <?php checked(esc_attr( get_the_author_meta( 'practice_gender_identity', $user->ID )), '1', true ); ?> />
				<?php } ?>
				<label class="description" for="practice-gender-identity">
					Gender Identity
				</label>
			</td>
		</tr>
		<tr>
			<td>
				<?php if ( get_the_author_meta( 'practice_grief', $user->ID ) ) { ?>
					<input id="practice-grief" name="practice-grief" type="checkbox" value="1" checked="checked" />
				<?php } else { ?>
					<input id="practice-grief" name="practice-grief" type="checkbox" value="1" <?php checked(esc_attr( get_the_author_meta( 'practice_grief', $user->ID )), '1', true ); ?> />
				<?php } ?>
				<label class="description" for="practice-grief">
					Grief/Loss
				</label>
			</td>
			<td>
				<?php if ( get_the_author_meta( 'practice_men_issues', $user->ID ) ) { ?>
					<input id="practice-men-issues" name="practice-men-issues" type="checkbox" value="1" checked="checked" />
				<?php } else { ?>
					<input id="practice-men-issues" name="practice-men-issues" type="checkbox" value="1" <?php checked(esc_attr( get_the_author_meta( 'practice_men_issues', $user->ID )), '1', true ); ?> />
				<?php } ?>
				<label class="description" for="practice-men-issues">
					Men's Issues
				</label>
			</td>
			<td>
				<?php if ( get_the_author_meta( 'practice_mood', $user->ID ) ) { ?>
					<input id="practice-mood" name="practice-mood" type="checkbox" value="1" checked="checked" />
				<?php } else { ?>
					<input id="practice-mood" name="practice-mood" type="checkbox" value="1" <?php checked(esc_attr( get_the_author_meta( 'practice_mood', $user->ID )), '1', true ); ?> />
				<?php } ?>
				<label class="description" for="practice-mood">
					Mood Disorders
				</label>
			</td>
			<td>
				<?php if ( get_the_author_meta( 'practice_personality', $user->ID ) ) { ?>
					<input id="practice-personality" name="practice-personality" type="checkbox" value="1" checked="checked" />
				<?php } else { ?>
					<input id="practice-personality" name="practice-personality" type="checkbox" value="1" <?php checked(esc_attr( get_the_author_meta( 'practice_personality', $user->ID )), '1', true ); ?> />
				<?php } ?>
				<label class="description" for="practice-personality">
					Personality Disorders
				</label>
			</td>
		</tr>
		<tr>
			<td>
				<?php if ( get_the_author_meta( 'practice_relationships', $user->ID ) ) { ?>
					<input id="practice-relationships" name="practice-relationships" type="checkbox" value="1" checked="checked" />
				<?php } else { ?>
					<input id="practice-relationships" name="practice-relationships" type="checkbox" value="1" <?php checked(esc_attr( get_the_author_meta( 'practice_relationships', $user->ID )), '1', true ); ?> />
				<?php } ?>
				<label class="description" for="practice-relationships">
					Relationships
				</label>
			</td>
			<td>
				<?php if ( get_the_author_meta( 'practice_trauma', $user->ID ) ) { ?>
					<input id="practice-trauma" name="practice-trauma" type="checkbox" value="1" checked="checked" />
				<?php } else { ?>
					<input id="practice-trauma" name="practice-trauma" type="checkbox" value="1" <?php checked(esc_attr( get_the_author_meta( 'practice_trauma', $user->ID )), '1', true ); ?> />
				<?php } ?>
				<label class="description" for="practice-trauma">
					Trauma
				</label>
			</td>
			<td>
				<?php if ( get_the_author_meta( 'practice_women_issues', $user->ID ) ) { ?>
					<input id="practice-women-issues" name="practice-women-issues" type="checkbox" value="1" checked="checked" />
				<?php } else { ?>
					<input id="practice-women-issues" name="practice-women-issues" type="checkbox" value="1" <?php checked(esc_attr( get_the_author_meta( 'practice_women_issues', $user->ID )), '1', true ); ?> />
				<?php } ?>
				<label class="description" for="practice-women-issues">
					Women's Issues
				</label>
			</td>
			<td>
				<?php if ( get_the_author_meta( 'practice_other', $user->ID ) ) { ?>
					<input id="practice-other" name="practice-other" type="checkbox" value="1" checked="checked" />
				<?php } else { ?>
					<input id="practice-other" name="practice-other" type="checkbox" value="1" <?php checked(esc_attr( get_the_author_meta( 'practice_other', $user->ID )), '1', true ); ?> />
				<?php } ?>
				<label class="description" for="practice-other">
					Other
				</label>
			</td>
		</tr>
	</table>




	<h3>Licensing</h3>
	<table class="form-table">
		<tr>
			<th><label for="license-degree">License Degree:</label></th>
			<td>
				<input type="text" name="license-degree" id="license-degree" value="<?php echo esc_attr( get_the_author_meta( 'license_degree', $user->ID ) ); ?>" class="regular-text" /><br />
			</td>
		</tr>
		<tr>
			<th><label for="license-current">License Current:</label></th>
			<td>
				<input type="text" name="license-current" id="license-current" value="<?php echo esc_attr( get_the_author_meta( 'license_current', $user->ID ) ); ?>" class="regular-text" /><br />
			</td>
		</tr>
		<tr>
			<th><label for="license-lcsw">LCSW License:</label></th>
			<td>
				<input type="text" name="license-lcsw" id="license-lcsw" value="<?php echo esc_attr( get_the_author_meta( 'license_lcsw', $user->ID ) ); ?>" class="regular-text" /><br />
			</td>
		</tr>
		<tr>
			<th><label for="license-lcsw-state">LCSW LicenseState:</label></th>
			<td>
				<input type="text" name="license-lcsw-state" id="license-lcsw-state" value="<?php echo esc_attr( get_the_author_meta( 'license_lcsw_state', $user->ID ) ); ?>" class="regular-text" /><br />
			</td>
		</tr>
	</table>

	<h3>Schooling - Current</h3>
	<table class="form-table">
		<tr>
			<th><label for="school-current-name">School Name:</label></th>
			<td>
				<input type="text" name="school-current-name" id="school-current-name" value="<?php echo esc_attr( get_the_author_meta( 'school_current_name', $user->ID ) ); ?>" class="regular-text" /><br />
			</td>
		</tr>
		<tr>
			<th><label for="school-current-graduation-date">Expected Graduation Date:</label></th>
			<td>
				<input type="text" name="school-current-graduation-date" id="school-current-graduation-date" value="<?php echo esc_attr( get_the_author_meta( 'school_current_graduation_date', $user->ID ) ); ?>" class="regular-text" /><br />
			</td>
		</tr>
	</table>


	<h3>Schooling - Undergraduate</h3>
	<table class="form-table">
		<tr>
			<th><label for="school-undergraduate-name">School Name:</label></th>
			<td>
				<input type="text" name="school-undergraduate-name" id="school-undergraduate-name" value="<?php echo esc_attr( get_the_author_meta( 'school_undergraduate_name', $user->ID ) ); ?>" class="regular-text" /><br />
			</td>
		</tr>
		<tr>
			<th><label for="school-undergraduate-major">Major:</label></th>
			<td>
				<input type="text" name="school-undergraduate-major" id="school-undergraduate-major" value="<?php echo esc_attr( get_the_author_meta( 'school_undergraduate_major', $user->ID ) ); ?>" class="regular-text" /><br />
			</td>
		</tr>
		<tr>
			<th><label for="school-undergraduate-degree">Degree:</label></th>
			<td>
				<input type="text" name="school-undergraduate-degree" id="school-undergraduate-degree" value="<?php echo esc_attr( get_the_author_meta( 'school_undergraduate_degree', $user->ID ) ); ?>" class="regular-text" /><br />
			</td>
		</tr>
		<tr>
			<th><label for="school-current-graduation-date">Expected Graduation Date:</label></th>
			<td>
				<input type="text" name="school-undergraduate-graduation-date" id="school-undergraduate-graduation-date" value="<?php echo esc_attr( get_the_author_meta( 'school_undergraduate_graduation_date', $user->ID ) ); ?>" class="regular-text" /><br />
			</td>
		</tr>
	</table>


	<h3>Schooling - Graduate</h3>
	<table class="form-table">
		<tr>
			<th><label for="school-graduate-name">School Name:</label></th>
			<td>
				<input type="text" name="school-graduate-name" id="school-graduate-name" value="<?php echo esc_attr( get_the_author_meta( 'school_graduate_name', $user->ID ) ); ?>" class="regular-text" /><br />
			</td>
		</tr>
		<tr>
			<th><label for="school-graduate-major">Major:</label></th>
			<td>
				<input type="text" name="school-graduate-major" id="school-graduate-major" value="<?php echo esc_attr( get_the_author_meta( 'school_graduate_major', $user->ID ) ); ?>" class="regular-text" /><br />
			</td>
		</tr>
		<tr>
			<th><label for="school-graduate-degree">Degree:</label></th>
			<td>
				<input type="text" name="school-graduate-degree" id="school-graduate-degree" value="<?php echo esc_attr( get_the_author_meta( 'school_graduate_degree', $user->ID ) ); ?>" class="regular-text" /><br />
			</td>
		</tr>
		<tr>
			<th><label for="school-graduate-graduation-date">Expected Graduation Date:</label></th>
			<td>
				<input type="text" name="school-graduate-graduation-date" id="school-graduate-graduation-date" value="<?php echo esc_attr( get_the_author_meta( 'school_graduate_graduation_date', $user->ID ) ); ?>" class="regular-text" /><br />
			</td>
		</tr>
	</table>


	<h3>Schooling - Other</h3>
	<table class="form-table">
		<tr>
			<th><label for="school-other-name">School Name:</label></th>
			<td>
				<input type="text" name="school-other-name" id="school-other-name" value="<?php echo esc_attr( get_the_author_meta( 'school_other_name', $user->ID ) ); ?>" class="regular-text" /><br />
			</td>
		</tr>
		<tr>
			<th><label for="school-other-major">Major:</label></th>
			<td>
				<input type="text" name="school-other-major" id="school-other-major" value="<?php echo esc_attr( get_the_author_meta( 'school_other_major', $user->ID ) ); ?>" class="regular-text" /><br />
			</td>
		</tr>
		<tr>
			<th><label for="school-other-degree">Degree:</label></th>
			<td>
				<input type="text" name="school-other-degree" id="school-other-degree" value="<?php echo esc_attr( get_the_author_meta( 'school_other_degree', $user->ID ) ); ?>" class="regular-text" /><br />
			</td>
		</tr>
		<tr>
			<th><label for="school-other-graduation-date">Expected Graduation Date:</label></th>
			<td>
				<input type="text" name="school-other-graduation-date" id="school-other-graduation-date" value="<?php echo esc_attr( get_the_author_meta( 'school_other_graduation_date', $user->ID ) ); ?>" class="regular-text" /><br />
			</td>
		</tr>
	</table>


	<!-- <h3>Felony</h3>
	<table class="form-table">
		<tr>
			<th><label for="have-felony">Do you have a felony?</label></th>
			<td>
				<?php
	            $selected = get_the_author_meta( 'have_felony', $user->ID );
	            ?>
	            <select name="have-felony" id="have-felony">
	                <option value="Yes" <?php // echo ($selected == "Yes")?  'selected="selected"' : '' ?>>Yes</option>
	                <option value="No" <?php // echo ($selected == "No")?  'selected="selected"' : '' ?>>No</option>
				</select>
			</td>
		</tr>
		<tr>
			<th><label for="felony-details">Felony Details:</label></th>
			<td>
				<textarea rows="6" type="textarea" name="felony-details" class="regular-text"><?php // echo esc_attr( get_the_author_meta( 'felony_details', $user->ID ) ); ?></textarea>
			</td>
		</tr>
		<tr>
			<th><label for="have-sanction">Sanction?</label></th>
			<td>
				<?php
	            $selected = get_the_author_meta( 'have_sanction', $user->ID );
	            ?>
	            <select name="have-sanction" id="have-sanction">
	                <option value="Yes" <?php // echo ($selected == "Yes")?  'selected="selected"' : '' ?>>Yes</option>
	                <option value="No" <?php // echo ($selected == "No")?  'selected="selected"' : '' ?>>No</option>
				</select>
			</td>
		</tr>
		<tr>
			<th><label for="sanction-details">Sanction Details:</label></th>
			<td>
				<textarea rows="6" type="textarea" name="sanction-details" class="regular-text"><?php // echo esc_attr( get_the_author_meta( 'sanction_details', $user->ID ) ); ?></textarea>
			</td>
		</tr>
	</table> -->

	<h3>Discovery</h3>
	<table class="form-table">
		<tr>
			<th><label for="find-gscsw">How did you find out about GSCSW?</label></th>
			<td>
				<input type="text" name="find-gscsw" id="find-gscsw" value="<?php echo esc_attr( get_the_author_meta( 'find_gscsw', $user->ID ) ); ?>" class="regular-text" /><br />
			</td>
		</tr>
	</table>


<?php } }


// Save custom meta fields when user updates a field
add_action( 'personal_options_update', 'my_save_extra_profile_fields' );
add_action( 'edit_user_profile_update', 'my_save_extra_profile_fields' );

function my_save_extra_profile_fields( $user_id ) {

	if ( !current_user_can( 'edit_user', $user_id ) )
		return false;

	// Basic Information
	update_usermeta( $user_id, 'renew_date', $_POST['renew-date'] );
	update_usermeta( $user_id, 'hide_email', $_POST['hide-email'] );
	update_usermeta( $user_id, 'accept_insurance', $_POST['accept-insurance'] );
	update_usermeta( $user_id, 'communication_preference', $_POST['communication-preference'] );
	update_usermeta( $user_id, 'mail_location', $_POST['mail-location'] );
	update_usermeta( $user_id, 'display_on_directory', $_POST['display-on-directory'] );

	// Home Information
	update_usermeta( $user_id, 'home_address', $_POST['home-address'] );
	update_usermeta( $user_id, 'home_address_two', $_POST['home-address-two'] );
	update_usermeta( $user_id, 'home_city', $_POST['home-city'] );
	update_usermeta( $user_id, 'home_state', $_POST['home-state'] );
	update_usermeta( $user_id, 'home_zip', $_POST['home-zip'] );
	// update_usermeta( $user_id, 'home_country', $_POST['home-country'] );
	update_usermeta( $user_id, 'home_phone', $_POST['home-phone'] );
	update_usermeta( $user_id, 'home_mobile_phone', $_POST['home-mobile-phone'] );
	update_usermeta( $user_id, 'home_fax', $_POST['home-fax'] );

	// Office Primary
	update_usermeta( $user_id, 'office_name', $_POST['office-name'] );
	update_usermeta( $user_id, 'office_address', $_POST['office-address'] );
	update_usermeta( $user_id, 'office_address_two', $_POST['office-address-two'] );
	update_usermeta( $user_id, 'office_city', $_POST['office-city'] );
	update_usermeta( $user_id, 'office_state', $_POST['office-state'] );
	update_usermeta( $user_id, 'office_zip', $_POST['office-zip'] );
	// update_usermeta( $user_id, 'office_country', $_POST['office-country'] );
	update_usermeta( $user_id, 'office_website_url', $_POST['office-website'] );
	update_usermeta( $user_id, 'office_phone', $_POST['office-phone'] );

	// Office Secondary
	update_usermeta( $user_id, 'office_two_name', $_POST['office-two-name'] );
	update_usermeta( $user_id, 'office_two_address', $_POST['office-two-address'] );
	update_usermeta( $user_id, 'office_two_address_two', $_POST['office-two-address-two'] );
	update_usermeta( $user_id, 'office_two_city', $_POST['office-two-city'] );
	update_usermeta( $user_id, 'office_two_state', $_POST['office-two-state'] );
	update_usermeta( $user_id, 'office_two_zip', $_POST['office-two-zip'] );
	// update_usermeta( $user_id, 'office_two_country', $_POST['office-two-country'] );
	update_usermeta( $user_id, 'office_two_website_url', $_POST['office-two-website'] );

	// Practice Description
	update_usermeta( $user_id, 'practice_short', $_POST['practice-short'] );
	update_usermeta( $user_id, 'practice_long', $_POST['practice-long'] );

	// Specialties
	update_usermeta( $user_id, 'practice_women_issues', $_POST['practice-women-issues'] );
	update_usermeta( $user_id, 'practice_men_issues', $_POST['practice-men-issues'] );
	update_usermeta( $user_id, 'practice_gender_identity', $_POST['practice-gender-identity'] );
	update_usermeta( $user_id, 'practice_gay_issues', $_POST['practice-gay-issues'] );
	update_usermeta( $user_id, 'practice_relationships', $_POST['practice-relationships'] );
	update_usermeta( $user_id, 'practice_trauma', $_POST['practice-trauma'] );
	update_usermeta( $user_id, 'practice_grief', $_POST['practice-grief'] );
	update_usermeta( $user_id, 'practice_adoption', $_POST['practice-adoption'] );
	update_usermeta( $user_id, 'practice_mood', $_POST['practice-mood'] );
	update_usermeta( $user_id, 'practice_dissociative', $_POST['practice-dissociative'] );
	update_usermeta( $user_id, 'practice_personality', $_POST['practice-personality'] );
	update_usermeta( $user_id, 'practice_add', $_POST['practice-add'] );
	update_usermeta( $user_id, 'practice_eating', $_POST['practice-eating'] );
	update_usermeta( $user_id, 'practice_addiction', $_POST['practice-addiction'] );
	update_usermeta( $user_id, 'practice_aging', $_POST['practice-aging'] );
	update_usermeta( $user_id, 'practice_other', $_POST['practice-other'] );


	// License Degree
	update_usermeta( $user_id, 'license_degree', $_POST['license-degree'] );
	update_usermeta( $user_id, 'license_current', $_POST['license-current'] );
	update_usermeta( $user_id, 'license_lcsw', $_POST['license-lcsw'] );
	update_usermeta( $user_id, 'license_lcsw_state', $_POST['license-lcsw-state'] );


	// Current School
	update_usermeta( $user_id, 'school_current_name', $_POST['school-current-name'] );
	update_usermeta( $user_id, 'school_current_graduation_date', $_POST['school-current-graduation-date'] );


	// Undergraduate School
	update_usermeta( $user_id, 'school_undergraduate_name', $_POST['school-undergraduate-name'] );
	update_usermeta( $user_id, 'school_undergraduate_major', $_POST['school-undergraduate-major'] );
	update_usermeta( $user_id, 'school_undergraduate_degree', $_POST['school-undergraduate-degree'] );
	update_usermeta( $user_id, 'school_undergraduate_graduation_date', $_POST['school-undergraduate-graduation-date'] );


	// Graduate School
	update_usermeta( $user_id, 'school_graduate_name', $_POST['school-graduate-name'] );
	update_usermeta( $user_id, 'school_graduate_major', $_POST['school-graduate-major'] );
	update_usermeta( $user_id, 'school_graduate_degree', $_POST['school-graduate-degree'] );
	update_usermeta( $user_id, 'school_graduate_graduation_date', $_POST['school-graduate-graduation-date'] );


	// Other School
	update_usermeta( $user_id, 'school_other_name', $_POST['school-other-name'] );
	update_usermeta( $user_id, 'school_other_major', $_POST['school-other-major'] );
	update_usermeta( $user_id, 'school_other_degree', $_POST['school-other-degree'] );
	update_usermeta( $user_id, 'school_other_graduation_date', $_POST['school-other-graduation-date'] );

	// Felony
	// update_usermeta( $user_id, 'have_felony', $_POST['have-felony'] );
	// update_usermeta( $user_id, 'felony_details', $_POST['felony-details'] );
	// update_usermeta( $user_id, 'have_sanction', $_POST['have-sanction'] );
	// update_usermeta( $user_id, 'sanction_details', $_POST['sanction-details'] );


	// Discovery
	update_usermeta( $user_id, 'find_gscsw', $_POST['find-gscsw'] );

}

?>
