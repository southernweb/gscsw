<div class="post-meta post-after-meta">

	<?php the_tags('Tags: ', ', ', '<br />'); ?>
	
	Posted in <?php the_category(', ') ?> | 
	
	<?php comments_popup_link('No Comments &#187;', '1 Comment &#187;', '% Comments &#187;'); ?>
	
	<?php if( is_user_logged_in() ) { ?> | 
		<?php edit_post_link('Edit post', '<span>', '</span>'); ?>
	<?php } ?>
	
</div>