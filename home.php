<?php get_header(); ?>
	
<div id="content">

	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

		<div <?php post_class() ?>>
		
			<h2 class="entry-title"><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h2>

			<?php get_template_part('inc/post', 'before-meta'); ?>
			
			<div class="entry-summary">
				<?php the_excerpt(); ?>
			</div>	
			
			<?php get_template_part('inc/post', 'after-meta'); ?>
			
		</div>

	<?php endwhile; ?>

	<?php 
		if( $wp_query->post_count > 10 ) {
			get_template_part('inc/post', 'nav');
		}		
	?>

	<?php else : ?>

		<h2>Not Found</h2>

	<?php endif; ?>

</div><!-- /#content -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>