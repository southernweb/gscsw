<?php get_header(); ?>

<div id="slider">
	<?php while(has_sub_field('slider')) : ?>
		<div class="slide">
			<div class="slide_image">
			<?php
				if(get_sub_field('slide_caption')) { echo '<div class="slide_caption">' . get_sub_field('slide_caption') . '</div>'; }
			?>
				<img src="<?php the_sub_field('slide_image'); ?>" width="600" height="380" alt=""/>
			</div>
			<div class="inner">
				<h2><?php the_sub_field('slide_headline'); ?></h2>
				<?php the_sub_field('slide_content'); ?>
				
				<!-- <p>On April 19, 2012, Governor Nathan Deal signed HB 434 into law.  Many years of work went into the passage of this bill, which affects the ability of social workers to diagnose.  We thank our lobbyist, Wendi Clifton, Katie M. Dempsey, R-Dist. 13 who sponsored the bill, and all of our members who answered the call to action and directly contacted your elected officials.  Our voice matters!</p> -->
			</div>

		</div>
	<?php endwhile; ?>
</div>

<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/jquery.cycle.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		jQuery('#slider').cycle({
			speed: 1500,
			timeout: 9500
		});
	});
</script>
		
<div id="content" class="clearfix">

	<?php 
		if (have_posts()) : while (have_posts()) : the_post();
			the_content();
		endwhile; endif; 
	?>
	
</div><!-- /#content -->
	
<?php get_sidebar(); ?>
<?php get_footer(); ?>