<?php 

/* Template Name: About */

get_header(); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		
	<div id="content" class="clearfix">
	
		<div <?php post_class(); ?>>
		
			<span class="entry-title hide"><?php the_title();?></span>
			
			<div class="entry-content">
				<?php the_content(); ?>

				<?php if( have_rows('member') ): ?>

					<div class="board-member-container">
						<?php while ( have_rows('member') ) : the_row(); ?>
						<?php
							$photo = get_sub_field( 'photo' );
							$photoUrl = $photo['sizes']['medium'];
						?>
							<div class="board-member-row">
								<div class="board-member-column board-member-img-column">
								<?php if($photoUrl){ ?>
									<img src="<?php echo $photoUrl ?>">
								<?php } else { ?>
									<div class="image-spacer"></div>		
								<?php } ?>
								</div>
								<div class="board-member-column">
									<div class="board-member-title">
										<?php the_sub_field('title'); ?>
									</div>
									<div class="board-member-name">
										<?php the_sub_field('name'); ?>
									</div>
								</div>
								<div class="board-member-column">
									<div class="board-member-email">
										<?php 

											$emailaddy = the_sub_field('email');

											if($emailaddy){	?>
												<a href="mailto:<?php echo antispambot( $emailaddy); ?>">
													<?php echo antispambot( $emailaddy); ?>
												</a> 
										<?php } ?>
									</div>
									<div class="board-member-phone">
										<?php if(the_sub_field('phone')){
											the_sub_field('phone');
										} ?>
									</div>
								</div>
							</div>
						<?php endwhile; ?>
					</div>
				<?php endif; ?>
				<hr />
				<?php // the_field('past_presidents'); ?>

				<h2>Past Presidents</h2>
				<?php if( have_rows('pastpresidents') ): ?>

						<table class="inline" style="height: 664px;" border="0" width="624" cellspacing="0" cellpadding="4">
							<tbody>
								<tr>
									<td width="264" height="16"><strong>Name</strong></td>
									<td width="119"><strong>Years Served</strong></td>
									<td width="156"></td>
								</tr>
								<?php while ( have_rows('pastpresidents') ) : the_row(); ?>
								<tr>
									<td width="264" height="16"><?php the_sub_field('name'); ?></td>
									<td width="119"><?php the_sub_field('years_served'); ?></td>
									<td width="156"></td>
								</tr>
								<?php endwhile; ?>
							</tbody>
						</table>

				<?php endif; ?>

			</div>
			
		</div>
		
	</div><!-- /#content -->

<?php endwhile; endif; ?>


	
<?php get_sidebar(); ?>
<?php get_footer(); ?>