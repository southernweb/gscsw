<?php get_header(); ?>

<div id="content" class="clearfix">

	<?php $post = $posts[0]; // Hack. Set $post so that the_date() works. ?>

	<?php /* If this is a category archive */ if (is_category()) { ?>
		<h1><em>Category Archive</em>: <?php single_cat_title(); ?></h1>

	<?php /* If this is a tag archive */ } elseif( is_tag() ) { ?>
		<h1>Posts Tagged &#8216;<?php single_tag_title(); ?>&#8217;</h1>

	<?php /* If this is a daily archive */ } elseif (is_day()) { ?>
		<h1>Archive for <?php the_time('F jS, Y'); ?></h1>

	<?php /* If this is a monthly archive */ } elseif (is_month()) { ?>
		<h1>Archive for <?php the_time('F, Y'); ?></h1>

	<?php /* If this is a yearly archive */ } elseif (is_year()) { ?>
		<h1>Archive for <?php the_time('Y'); ?></h1>

	<?php /* If this is an author archive */ } elseif (is_author()) { ?>
		<h1>Author Archive</h1>

	<?php /* If this is a paged archive */ } elseif (isset($_GET['paged']) && !empty($_GET['paged'])) { ?>
		<h1>Blog Archives</h1>
	
	<?php } ?>

	<?php if (have_posts()) : ?>

		<?php while (have_posts()) : the_post(); ?>
		
			<div class="entry clearfix">
			
				<h2><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h2>
				
				<?php get_template_part('inc/post', 'before-meta'); ?>
								
				<?php the_excerpt(); ?>
				
				<?php get_template_part('inc/post', 'after-meta'); ?>
				
				<hr />
				
			</div><!-- /.entry -->

		<?php endwhile; ?>

		<?php get_template_part('inc/post', 'nav'); ?>
		
	<?php else : ?>

		<div class="entry">
		
			<p>Nothing found</p>
			
		</div><!-- /.entry -->

	<?php endif; ?>

</div><!-- /#content -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
