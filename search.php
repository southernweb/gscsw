<?php get_header(); ?>

<div id="content" class="clearfix">

	<h1>Search Results</h1>

	<?php if (have_posts()) : ?>

		<?php get_template_part('inc/post', 'nav'); ?>

		<?php while (have_posts()) : the_post(); ?>

			<div class="entry">

				<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>

				<?php //get_template_part('inc/post', 'before-meta'); ?>

				<?php the_excerpt(); ?>

				<?php //get_template_part('inc/post', 'after-meta'); ?>
				<hr/>
			</div>

		<?php endwhile; ?>

		<?php get_template_part('inc/post', 'nav'); ?>

	<?php else : ?>

		<div class="entry">
			<p>No results found.</p>
		</div>

	<?php endif; ?>

</div><!-- /#content -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
